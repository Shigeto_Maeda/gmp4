##########################################################################
#
#    Generative Modeling Project 4 for Blender 2.68a
#    Coded by Shigeto 'Shige' Maeda  2013-07-31
#    s.maeda@oud-japan.co.jp
#
#    ! Make a 'Cube' or something object first, and 'Generate' !
#
##########################################################################

from bpy import *
import bpy
import math, mathutils, random

matNum = 0
mData = []

class GMP_Panel(bpy.types.Panel):
	bl_label = "*** Generator"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	
	def draw(self, context):
		layout = self.layout
		
		scn = bpy.context.scene
		
		row = layout.row()
#		'''
 #		row = row.column()
 #		row.prop( scn, "Body_Amount" )

		row = row.column()
		row.label(text="Body parameter")
		
		row = row.column()
		row.prop( scn, "Body_Len" )
		
#		'''
		row = row.column()
		row.label(text=" ")
		
		row.operator( "bpt.generate_op" )

class GMP_Operator(bpy.types.Operator):
	bl_idname = "bpt.generate_op"
	bl_label = "Generate"
 
	def execute(self, context):
#		bpy.ops.object.select_all(action='SELECT')
#		bpy.ops.object.delete()

		Body_Len = bpy.context.scene.Body_Len #1
		
#		bpy.ops.object.select_all(action='SELECT')
#		bpy.ops.object.delete(use_global=False)
				
		data = [Body_Len] 

		generate()
			
		#bpy.ops.object.hide_view_clear()
		
		#self.report( "INFO", "New Life was Generated!" )
		return {'FINISHED'}

def register():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )
 
def unregister():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )

#------------------------------------------------------------------------------------ Main Code from here
# extrudeOneStep ---------------------------------------->
def extrudeOneStep(oneStep):
	dZ = oneStep[0]
	sX = oneStep[1]
	sY = oneStep[2]
	rA = oneStep[3]
	aX = oneStep[4]
	aY = oneStep[5]
	aZ = oneStep[6]
	mLevel = oneStep[7]
	groupNum = oneStep[8]
	n = oneStep[9]
	
	selMode2FA()
	bpy.ops.object.mode_set(mode='EDIT')
	ob = bpy.context.object
	# Scale control by face(area) size-->
	context = bpy.context
	if (context.active_object != None):
		object = bpy.context.active_object
	
	# Get the mesh
	me = ob.data
	# Loop through the faces to find the center
	rotDir = 1	
				
	for p in me.polygons:
		if p.select == True:
			p_center = [0.0, 0.0, 0.0]
			for v in p.vertices:
				p_center[0] += me.vertices[v].co[0]        
#				p_center[1] += me.vertices[v].co[1]
#				p_center[2] += me.vertices[v].co[2]
			p_center[0] /= len(p.vertices) # division by zero               
#			p_center[1] /= len(p.vertices) # division by zero               
#			p_center[2] /= len(p.vertices) # division by zero       
			if p_center[0] > 0.0:
				rotDir = 1.0
			else:
				rotDir = -1.0
				
	bpy.ops.transform.shrink_fatten(value=dZ * sX, mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1.0, snap=False, snap_target='CLOSEST', snap_point=(0.0, 0.0, 0.0), snap_align=False, snap_normal=(0.0, 0.0, 0.0), release_confirm=False)			
	bpy.ops.transform.resize(value=(sX, sX, sX), constraint_axis=(False, False, False), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
	bpy.ops.transform.rotate(value=(rA*rotDir), axis=(aX, aY, aZ), constraint_axis=(False, False, False), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), release_confirm=False)
	
	bpy.ops.object.vertex_group_remove_from(use_all_groups=True)
	
#	if mLevel == 1:
#		vGroup = 'temp'
#		ob = bpy.context.object
#		newGroup = ob.vertex_groups.new(vGroup)
#		bpy.ops.object.vertex_group_set_active(group=vGroup)
#		bpy.ops.object.mode_set(mode='EDIT')
#		bpy.ops.object.vertex_group_assign()
	
#		for i in range(matNum - 1):
#			vGroup = 'mtrl' + str(i + 1)
#			removeVGroupFrom(['temp', vGroup])
		
#		vGroup = 'temp'
#		ob = bpy.context.object
#		bpy.ops.object.vertex_group_set_active(group=vGroup)
#		bpy.ops.object.vertex_group_remove()
		
#	elif mLevel == 2 or mLevel == 3:
#		vGroup = 'temp'
#		ob = bpy.context.object
#		newGroup = ob.vertex_groups.new(vGroup)
#		bpy.ops.object.vertex_group_set_active(group=vGroup)
#		bpy.ops.object.mode_set(mode='EDIT')
#		bpy.ops.object.vertex_group_assign()
		
#		for i in range(n + 1, groupNum):
#			vGroup = 'mygroup' + str(i)
#			removeVGroupFrom(['temp', vGroup])
		
#		vGroup = 'temp'
#		ob = bpy.context.object
#		bpy.ops.object.vertex_group_set_active(group=vGroup)
#		bpy.ops.object.vertex_group_remove()
	# Redraw the screen
	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
	
# extrudeOneStep(END) ----------------------------------------<

# extrudeFacesByNum(Multi Step) ------------------------------------------------------->
def extrudeFacesByNum(mltData):
	mNum = mltData[0] 
	mainLevel = mltData[1] 
	stepNum = mltData[2] 
	stepDist = mltData[3] 
	stepRot = mltData[4] 
	startDeg = mltData[5] 
	stepDeg = mltData[6] 
	modLevel = mltData[7] 
	modRate = mltData[8]
	axis = mltData[9]
	jointNum = mltData[10]
	fPers = mltData[11]
	mLevel = mltData[12]
	
	ob = bpy.context.object
	# Make sure nothing is selected
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	selMode2FA()
	# Select the vertex group
	if mLevel == 1:
		selPart(mNum)
	else:
		selPartByNum(mNum)
	
	# Go into object mode so that we are sure we have the current list of faces 
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# Make a nice list to keep the vertex groups in
	groupList = []
				
	# Now loop through all the faces
	fNum = 0
	for i, p in enumerate(ob.data.polygons):
		
		# If this face is selected
		if ob.data.polygons[i].select == True:

			# Create a new vertex group!
			vGroup = 'mygroup' + str(fNum)
			ob = bpy.context.object
			newGroup = ob.vertex_groups.new(vGroup)
			bpy.ops.object.vertex_group_set_active(group=vGroup)
			bpy.ops.object.vertex_group_remove(all=False)
			newGroup = ob.vertex_groups.new(vGroup)
			bpy.ops.object.vertex_group_set_active(group=vGroup)
			groupList.append(newGroup)
			# Get all the vertices in the current face and add them to the new group
			for v in p.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			fNum += 1
		#Check the group num
#		print('Material = ', mNum, ': Face = ', fNum)
		
	# Now we loop through the groups and do what we want.
	n = 0
	for g in groupList:
		
		# Make sure nothing is selected
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		t = startDeg
		oldValue = 1.0
		oldValue2 = 1.0
		# Shape
		j = 0
		if stepNum == 99:
			RandStepNum = random.randint(6, 20)
		else:
			RandStepNum = stepNum
			
		#dirFace = -1**(random.randint(0, 2))
		dirFace = random.uniform(0.3, 1.0)**random.randint(0, 2)
		axis = [random.uniform(-1.0,1.0),random.uniform(-1.0,1.0),random.uniform(-1.0,1.0)]
		for i in range(RandStepNum):
     
			bpy.ops.mesh.extrude_region(mirror=False)
			
			# An added trick... remove the extruded face from all vertex groups after the first extrusion (i == 0)
			# This way it can't get extruded again
			# Because the edge of the first face can be part of multiple groups
			if not i:
				bpy.ops.object.vertex_group_remove_from()
		
			bpy.ops.object.mode_set(mode='OBJECT')
			bpy.ops.object.mode_set(mode='EDIT')
		
			newValue = (math.sin(math.radians(t)+math.sin(math.radians(t/2.3)))+2.0)*mainLevel
			#newValue2 = (math.sin(math.radians(t*1.2)+math.sin(math.radians(t/1.3)))+2.0)*mainLevel
			newRatio = newValue/oldValue
			#newRatio2 = newValue2/oldValue2
				
			# Get the mesh
			me = ob.data
			# Loop through the faces to find the center
			area = 1
			for p in me.polygons:
#				if p.select == True and vGroup != 'body' and vGroup != 'spine' and vGroup != 'leg' and vGroup != 'root':
				if p.select == True:
					area = math.sqrt(p.area) * 3
			
			dZ = stepDist * area
			sX = newRatio
			sY = newRatio
			rA = modLevel*math.sin(math.radians(t*modRate))*(-stepRot/10)*dirFace
			aX = axis[0]
			aY = axis[1]
			aZ = axis[2]
			gNum = len(groupList)
			
			snglData = [dZ, sX, sY, rA, aX, aY, aZ, mLevel, gNum, n]
			extrudeOneStep(snglData)
			
			if jointNum != 0:
				j += 1
				if j == jointNum:
					bpy.ops.mesh.extrude_region(mirror=False)
					snglData = [-0.01, 0.6, 0.6, 0, aX, aY, aZ, mLevel, gNum, n]
					extrudeOneStep(snglData) # <-------------0
					bpy.ops.mesh.extrude_region(mirror=False)
					snglData = [-0.01, 1.66, 1.66, 0, aX, aY, aZ, mLevel, gNum, n]
					extrudeOneStep(snglData) # <-------------0
					j = 0
			t += stepDeg
			oldValue = newValue
			#oldValue2 = newValue2
		if RandStepNum != 0 and mLevel == 1:
			bpy.context.object.active_material_index = 24
			bpy.ops.object.material_slot_assign()
			
		if RandStepNum != 0 and mLevel == 3:
			bpy.context.object.active_material_index = 3
			bpy.ops.object.material_slot_assign()
	
		n += 1
	
# extrudeFacesByNum!(END) --------------------------------------------------<

#SelectMode->FACE
def selMode2FA():
	ob = bpy.context.object
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(type='FACE')
	bpy.ops.object.mode_set(mode='OBJECT')
	
#SelectMode->VERT
def selMode2VT():
	ob = bpy.context.object
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(type='VERT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
#SelectMode->Edit
def selMode2Edit():
	object = bpy.context.active_object
	bpy.ops.object.mode_set(mode='EDIT')

#SelectMode->Object
def selMode2Object():
	object = bpy.context.active_object
	bpy.ops.object.mode_set(mode='OBJECT')

# deselectAllMesh------------------------------------------>
def deselectAllMesh():
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')

#AddSubsurfModifier
def addSubsurf(level):
	bpy.ops.object.modifier_add(type='SUBSURF')
	object = bpy.context.active_object
	object.modifiers['Subsurf'].levels = level

#Make materials
def makeMaterials(num):
	for i in range(num):
		matNum = i	
		matName = 'material' + str(matNum)

		mat = bpy.data.materials.new(matName)
		mat.diffuse_color = (random.random(), random.random(), random.random())
		mat.texture_slots.add()

		obj = bpy.context.scene.objects.active
		obj.data.materials.append(mat)
	
	matName = 'temp'

	mat = bpy.data.materials.new(matName)
	mat.diffuse_color = (random.random(), random.random(), random.random())
	mat.texture_slots.add()

	obj = bpy.context.scene.objects.active
	obj.data.materials.append(mat)

#Make morph data
def makeMorphData(matNum):
	random.seed()
	mData = []
	for i in range(matNum):
		
		vGroup = 'mtrl' + str(i)
		mainLev = random.uniform(0.3, 0.4) #(0.3, 0.5)
		stNum = random.randint(3, 5)
		stDist = random.uniform(-0.3, -0.25)
		stRot = random.uniform(-3.0, 3.0)
		staDeg = random.uniform(30.0, 240.0) #(0, 360)
		stDeg = random.uniform(30.0, 120.0)
		modLev = random.uniform(2.0, 5.0)
		modRt = random.uniform(-1.0, 1.0)
#		axis = [random.uniform(-1, 0), random.uniform(-1, 0), random.uniform(-1, 0)]
		axis = [-2.22045e-16, 0, -1]
		jNum = 0
		fPers = random.uniform(0.0, 0.2)
		mLevel = 0
		dataList = [vGroup, mainLev, stNum, stDist, stRot, staDeg, stDeg, modLev, modRt, axis, jNum, fPers, mLevel]
		
		mData.append(dataList)
	
	return mData	
	
# select faces from Groups --------------------------------------------->
def selPart(vGroup):
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_set_active(group=vGroup)
	bpy.ops.object.vertex_group_select()
	
# select faces from By materialNum --------------------------------------------->
def selPartByNum(mNum):
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.context.object.active_material_index = mNum
	bpy.ops.object.material_slot_select()
	bpy.ops.object.mode_set(mode='OBJECT')

# Remove extruded faces from mtrl group
#def removeVGroupFrom(removeAfromB):
#	vGroupA = removeAfromB[0]
#	vGroupB = removeAfromB[1]
	
#	bpy.ops.object.mode_set(mode='EDIT')
#	bpy.ops.mesh.select_all(action='DESELECT')
#	bpy.ops.object.vertex_group_set_active(group=vGroupA)
#	bpy.ops.object.vertex_group_select()

#	bpy.ops.object.vertex_group_set_active(group=vGroupB)
#	bpy.ops.object.vertex_group_remove_from()

# Generate Object!!! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
def generate():
	for item in bpy.data.materials:
		ob = bpy.context.object
		bpy.data.materials.remove(item)

	obX = 0
	obY = 0
#	bodyLen = data[1]
			
#	bpy.ops.mesh.primitive_cube_add(radius=1, view_align=False, enter_editmode=False, location=(obX, obY, 0), rotation=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False)) ##matNum 24
	
#	bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=1, size=1, view_align=False, enter_editmode=False, location=(0, 0, 0), rotation=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False)) ##matNum 20

#	bpy.ops.mesh.primitive_torus_add(view_align=False, location=(0, 0, 0), rotation=(0, 0, 0), major_radius=1.3009, minor_radius=-0.716153, major_segments=5, minor_segments=3, use_abso=True, abso_major_rad=0.584742, abso_minor_rad=2.01705) ##matNum 15

	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

	addSubsurf(1)
	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")
	
	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
	
	deselectAllMesh()
	ob = bpy.context.object
	me = ob.data
	print(len(me.polygons))
	
	matNum = len(me.polygons)
	
	makeMaterials(matNum)
	mData = makeMorphData(matNum)
		
#	selMode2Edit
	for p in me.polygons:
		p.material_index = p.index
	selMode2FA()
	deselectAllMesh()
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='SELECT')
	bpy.ops.mesh.normals_make_consistent(inside=False)
	bpy.ops.mesh.select_all(action='DESELECT')
	
# morph level 1
	mLevel = 1
	for i in range(matNum):
		vGroup = 'mtrl' + str(i)
		ob = bpy.context.object
		newGroup = ob.vertex_groups.new(vGroup)
		bpy.ops.object.vertex_group_set_active(group=vGroup)
		ob.active_material_index = i
		bpy.ops.object.material_slot_select()
		bpy.ops.object.vertex_group_assign()
		bpy.ops.mesh.select_all(action='DESELECT')
		
	k = 0
	for p in me.polygons:
		l = k % matNum
		morphData = mData[l]
		morphData[12] = mLevel
		extrudeFacesByNum(morphData)
		k = k + 1
	
	print('mLevel 1 done') ##########
	
	bpy.ops.object.vertex_group_remove(all=True)
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	addSubsurf(1)
	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")
	bpy.ops.object.mode_set(mode='EDIT')
#	bpy.ops.mesh.select_all(action='SELECT')
#	bpy.ops.mesh.sort_elements(type='VIEW_ZAXIS', elements={'FACE'}, reverse=True, seed=0)
#	bpy.ops.mesh.select_all(action='DESELECT')
	
	mLevel = 2
	
#	Set All faces to material0 without Tips
	
	me = ob.data
	bpy.ops.mesh.select_all(action='SELECT')
	bpy.context.object.active_material_index = 24
	bpy.ops.object.material_slot_deselect()
	bpy.context.object.active_material_index = 0
	bpy.ops.object.material_slot_assign()
	bpy.ops.mesh.select_all(action='DESELECT')
	
######	 Spine on the Core - Selection
	bpy.ops.object.mode_set(mode='OBJECT')
	for p in me.polygons:
		p_center = [0.0, 0.0, 0.0]
		for v in p.vertices:
			p_center[0] += me.vertices[v].co[0]        
			p_center[1] += me.vertices[v].co[1]
			p_center[2] += me.vertices[v].co[2]
		p_center[0] /= len(p.vertices) # division by zero               
		p_center[1] /= len(p.vertices) # division by zero               
		p_center[2] /= len(p.vertices) # division by zero
		if p.index % 3 == 1 and (p_center[0]**2 + p_center[1]**2 + p_center[2]**2) < 3.5 and p.material_index != matNum:
#		if (p_center[0]**2 + p_center[1]**2 + p_center[2]**2) < 2 and p.material_index != matNum:
			p.material_index = 1 #1
	bpy.ops.object.mode_set(mode='EDIT')
	
###### Polyp on the Arm - Selection
#	bpy.ops.mesh.select_all(action='SELECT')
#	bpy.ops.mesh.sort_elements(type='VIEW_ZAXIS', elements={'FACE'}, reverse=True, seed=0)
	bpy.ops.mesh.select_all(action='DESELECT')
	
	bpy.ops.object.mode_set(mode='OBJECT')
	for p in me.polygons:
		p_center = [0.0, 0.0, 0.0]
		for v in p.vertices:
			p_center[0] += me.vertices[v].co[0]        
			p_center[1] += me.vertices[v].co[1]
			p_center[2] += me.vertices[v].co[2]
		p_center[0] /= len(p.vertices) # division by zero               
		p_center[1] /= len(p.vertices) # division by zero               
		p_center[2] /= len(p.vertices) # division by zero
		if p.index % 4 == 0 and p.area > 0.15 and p.material_index != 1 and p.material_index != 24:
			p.material_index = 2
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	
####------	Tentacles on the Arm
	
#	morphData = [24, 0.4, 5, -0.5, 40.0, 60.0, 20.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 2]
	morphData = [24, 0.5, 5, -0.5, 20.0, 120.0, 120.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 2]
	bpy.ops.object.mode_set(mode='OBJECT')
	extrudeFacesByNum(morphData)
	#bpy.ops.object.mode_set(mode='EDIT')
	
	print('tentacles done')
	
####------ Spine on the Core
	
	morphData = [1, 0.3, 2, -0.1, 20, 60, 120, 0.3, 0.3, [-2.22045e-16, -1, -0], 0, 0, 2]
	bpy.ops.object.mode_set(mode='OBJECT')
	extrudeFacesByNum(morphData)
	#bpy.ops.object.mode_set(mode='EDIT')
	
	print('spine done')
	
####------ Polyp base on the Arm

	morphData = [2, 0.5, 2, -0.1, 5, 60.0, 120.0, 0.0, 0.0, [-2.22045e-16, -1, -0], 0, 0, 3]
	bpy.ops.object.mode_set(mode='OBJECT')
	extrudeFacesByNum(morphData)
	#bpy.ops.object.mode_set(mode='EDIT')
	
	print('polyp base done')
	
####------ Polyp body on the Arm
	
#	morphData = ['root', 0.2, 3, -0.12, 20, 0, 0, 0, 0, [-2.22045e-16, -1, -0], 0, 0, 2]
#	morphData = ['root', 0.7, 5, -0.4, 20.0, 50.0, 50.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 3]
	morphData = [3, 0.8, 5, -0.3, 20.0, 60.0, 120.0, 0.3, 0.3, [-1, 0, 0], 2, 0, 3]
	bpy.ops.object.mode_set(mode='OBJECT')
	extrudeFacesByNum(morphData)
	#bpy.ops.object.mode_set(mode='EDIT')
	
	print('polyps done')
	
####------ Division for next

#	selMode2Object()
#	addSubsurf(1)
#	bpy.ops.object.mode_set(mode='OBJECT')
#	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")
#	bpy.ops.object.mode_set(mode='EDIT')
	
####------ Polyp Arm

	morphData = [3, 0.1, 2, 0.2, 40.0, 50.0, 80.0, 0.2, 0.2, [-1, 0, 0], 0, 0, 2]
	bpy.ops.object.mode_set(mode='OBJECT')
	extrudeFacesByNum(morphData) ########################
	#bpy.ops.object.mode_set(mode='EDIT')
	
	print('polyp arms done')

# Delete all material slots and materials
	bpy.ops.object.mode_set(mode='OBJECT')
		
	for i in range(matNum + 1):
		ob.active_material_index = i
		bpy.ops.object.material_slot_remove()
		
	for item in bpy.data.materials:
		ob = bpy.context.object
		bpy.data.materials.remove(item)
		
	addSubsurf(2)
	bpy.ops.object.shade_smooth()

#	bpy.ops.object.hide_view_set(unselected=False)

#------------------------------------------------------------------------------------ Main Code to here

if __name__ == '__main__':
#	'''

	bpy.types.Scene.Body_Len = bpy.props.IntProperty(name="Body_Len", default = 9, min = 2, max = 12, description = "Body Len")

#	'''
	register()